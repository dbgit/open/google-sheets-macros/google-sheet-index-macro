import Sheet = GoogleAppsScript.Spreadsheet.Sheet;

class IndexUpdates {
  private spreadSheet: GoogleAppsScript.Spreadsheet.Spreadsheet;

  constructor() {
    this.spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  }

  updateSheets() {
    const sheets = this.spreadSheet.getSheets();
    const indexSheet = sheets.find(sheet => sheet.getName() === 'Index');
    if (!indexSheet) {
      Browser.msgBox('Es gibt keinen sheet mit dem namen "Index"');
      return;
    }

    const indexedSheets = this.findSheetNames(indexSheet).map(sheet => this.updateSheetTitles(sheet, indexSheet));

    indexedSheets.forEach(this.updateSheetBanner)

    const currentIndexedSheetIds = indexedSheets.map(sheet => sheet.id);
    currentIndexedSheetIds.push(indexSheet.getSheetId());

    const notIndexedSheets = this.spreadSheet.getSheets().filter(sheet => !currentIndexedSheetIds.includes(sheet.getSheetId()));

    if (notIndexedSheets.length > 0) {
      const answer = Browser.msgBox('Es gibt nicht indexierte sheets, möchtest du diesen zum index hinzufügen?', Browser.Buttons.YES_NO)
      if (answer === 'yes') {
        this.addNotIndexItems(notIndexedSheets, indexSheet, indexedSheets[indexedSheets.length - 1].row + 1);
      }
    }

    this.spreadSheet.setActiveSheet(indexSheet);

    return;
  }

  findSheetNames(indexSheet: Sheet): SheetData[] {
    let currentRow = 13;
    const proposedSheets: SheetData[] = [];
    while (true) {
      const proposedCellName = indexSheet.getRange(currentRow, 1).getValue() as string;
      if (!proposedCellName) {
        break;
      }

      const data: SheetData = {
        name: proposedCellName,
        row: currentRow
      }

      const proposedSheetHyperlink = indexSheet.getRange(currentRow, 2).getFormula();
      const parsedHyperlink = proposedSheetHyperlink.match(/=(hyperlink|HYPERLINK)\("#gid=(?<SheetId>\w+)",\s*"(?<SheetName>[\w\s]+)"\)/u);
      if (parsedHyperlink?.groups) {
        const { SheetId } = parsedHyperlink.groups;
        const sheet = this.spreadSheet.getSheets().find(sheet => sheet.getSheetId() === Number(SheetId));
        if (sheet) {
          data.sheet = sheet;
        }
      }

      proposedSheets.push(data);
      currentRow++;
    }

    return proposedSheets;
  }

  updateSheetTitles(sheetData: SheetData, indexSheet: Sheet): CreatedSheetData {
    const { name, row } = sheetData;

    if (!sheetData.sheet) {
      sheetData.sheet = this.spreadSheet.insertSheet()
    }
    const { sheet } = sheetData;
    sheet.setName(name);
    indexSheet.getRange(row, 2).setFormula(this.createSheetHyperlink(sheet));

    sheetData.id = sheet?.getSheetId();

    return sheetData as CreatedSheetData;
  }

  updateSheetBanner(sheetData: CreatedSheetData): void {
    const { name, sheet } = sheetData;

    const range = sheet.getRange('A2:F2');
    range.merge();
    range.setValue(name);
    range.setBackground('#1c4587')
    range.setFontColor('#FFFFFF')
    range.setFontSize(20);
    range.setFontFamily('Arial');
  }

  addNotIndexItems(notIndex: Sheet[], indexSheet: Sheet, row: number = 13) {
    for (const sheet of notIndex) {
      const data: CreatedSheetData = {
        name: sheet.getName(),
        id: sheet.getSheetId(),
        row: row,
        sheet: sheet
      }

      indexSheet.getRange(row, 1).setValue(data.name);
      indexSheet.getRange(row, 2).setFormula(this.createSheetHyperlink(sheet));

      this.updateSheetBanner(data);

      row++;
    }
  }

  createSheetHyperlink(sheet: Sheet): string {
    return `=HYPERLINK("#gid=${sheet.getSheetId()}","${sheet.getSheetName()}")`;
  }
}

const updateIndex = () => new IndexUpdates().updateSheets();

interface SheetData {
  name: string;
  row: number;
  id?: number;
  sheet?: Sheet;
}

interface CreatedSheetData extends SheetData{
  id: number;
  sheet: Sheet;
}

